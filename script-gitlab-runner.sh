# atualizando
sudo apt update
sudo apt upgrade

# instalando o gitlab-runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

sudo gitlab-runner register

sudo gitlab-runner start

# instalar o docker
sudo apt install docker-compose

# alterando permisões
cat /etc/passwd
sudo usermod -aG docker gcp
sudo usermod -aG docker gitlab-runner

# logar no docker nas contas gcp e gitlab-runner
sudo su
su gitlab-runner
docker login
# >> informar user e password

su gcp
docker login
# >> informar user e password